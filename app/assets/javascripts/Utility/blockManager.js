// class to manage block objects
function BlockManager(slide){
  var blockList = [];
  var activeBlock = null;
  var copyStack = null;
  this.slide = slide;
  this.addBlock = function(block){
    blockList.push(block);
    this.activate(block);
  }


  this.removeBlock = function(block){
    Tree.tree.delete_node(block.id)
    for(var i = 0; i < blockList.length; i++){
      if (block === blockList[i]){
        activeBlock = null;
        if (block instanceof TextBlock){
          Utility.blockContentDecorator.deleteQuillToolbar(block.id);
        }
        return blockList.removeAt(i);
      }
    }
    return null;
  }

  this.isInBlock = function(block){
    for(var i = 0; i < blockList.length; i++){
      if (block === blockList[i])
        return true;
    }
    return false;
  }

  this.activate = function(block){
    if (this.isInBlock(block)){
      activeBlock = block;
      Utility.bindBlockDecoratorTo(activeBlock.$body);
      if (activeBlock instanceof TextBlock){
        Utility.bindBlockContentDecoratorTo(activeBlock.textEditor, activeBlock.id);
      }
      return true;
    }
    return false;
  }

  this.provideID = function(){
    // this not correct now. should be sent by webserver when backend is added
    return Math.floor((Math.random() * 10000) + 1);
  }

  this.getBlockTree = function(){
    return $.map(blockList, function(block){
      return block.getTreeJson();
    })
  }
}
