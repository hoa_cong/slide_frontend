var slideJson = {
  id          : "string",
  text        : "string",
  icon        : "string",
  state       : {
    opened    : true,
    disabled  : false,
    selected  : false
  },
  children    : [],
  li_attr     : {},
  a_attr      : {}
}
