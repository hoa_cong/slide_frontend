function BlockImageManager(){
  this.group = {};
  this.activeBlock = null;

  this.addBlock = function(image, id){
    this.group[id] = image;
  }

  this.removeBlock = function(id){
    Tree.tree.delete_node(id);
    delete this.group[id]
  }

  this.activate = function(image){
    this.activeBlock = image;
    Utility.bindBlockDecoratorTo(this.activeBlock.$body);
  }
}



BlockImageManager.prototype.provideID = function(){
  // this not correct now. should be sent by webserver when backend is added
  return Math.floor((Math.random() * 10000) + 1);
}
