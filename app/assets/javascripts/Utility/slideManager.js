var SlideManager = function(slideObjects){
  var self = this;
  this.$slideTree = $('#slide-tree');
  this.slides = [];
  this.activeSlide = null;
  if (slideObjects.length == 0){
    var id = this.provideID();
    var newSlide = new Slide(id, this);
    this.slides.push(newSlide);
    this.activeSlide = newSlide;
  }else{
    for (var i = 0; i < slideObjects.length; i++){
      var newSlide = new Slide(this.provideID(), this, slideObjects[i]);
      this.slides.push(newSlide);
    }
    this.activate(this.slides[0]);
  }
  Tree.initTree(this.getSlideTree());
}

SlideManager.prototype.activate = function(slide){
  this.activeSlide = slide;
}

SlideManager.prototype.addNewSlideAt = function(position){
  var newSlide =  new Slide(this.provideID(), this);
  this.slides = this.slides.insertAt(position-1, newSlide);
  this.activate(newSlide);
  this.drawTree();
}

SlideManager.prototype.removeSlide = function(slide){
  for (var i = 0; i < this.slides.length; i++){
    if (this.slides[i] === slide){
      this.slides = this.slides.removeAt(i);
      if (i == 0){
        this.activate(this.slides[0]);
      }
      else{
        this.activate(this.slides[i - 1]);
      }
      this.drawTree();
      break;
    }
  }
}

SlideManager.prototype.switchPosition = function(from, to){

}


// temporary id provider -> should be sent from backend
SlideManager.prototype.provideID = function(){
  return Math.floor((Math.random() * 10000) + 1);
}

SlideManager.prototype.getSlideTree = function(){
  return $.map(this.slides, function(slide){
    return slide.getTreeJson();
  })
}

SlideManager.prototype.addSlide = function(position){
  var id = this.provideID();
  var newSlide = new Slide(id, this);
  this.slides.push(newSlide);
  this.activeSlide = newSlide;
  //tbd
  Utility.rootNode.addChildren()
  Tree.tree.create_node('root', newSlide.getTreeJson());
}
