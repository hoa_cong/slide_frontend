function ImageBlock($parent, manager){
  this.$parent = $parent
  this.manager = manager
  var self = this;

  //add image block to parent:
  $parent.append(imageBlockHtml());
  this.$block = $parent.find('.image-block-container').last();
  this.$toolbar = this.$block.find('.image-block-toolbar');
  this.$closeButton = this.$block.find('.close-block-icon');
  this.$uploadFile = this.$block.find('.upload-image-file')
  this.$uploadUrl = this.$block.find('.upload-image-url');
  this.$uploadInput = this.$block.find('.image-url-input');
  this.$uploadButton = this.$block.find('.apply-url-button')
  this.$drag = this.$block.find('.image-block-cover');
  this.$body = this.$block.find('.image-block-body');
  this.$img = this.$block.find('.image-block-body img');
  this.$cover = this.$block.find('.image-block-cover')
  this.id = manager.provideID();
  this.manager.addBlock(this, this.id);


  function imageBlockHtml(){
    var html = ''
    html += '<div class="image-block-container">'
    html +=   '<i class="close-block-icon fa fa-close"></i>'
    html +=   '<i class="upload-image-file fa fa-upload"></i>'
    html +=   '<i class="upload-image-url fa fa-file-image-o"></i>'
    html +=   '<input class="image-url-input"></input>'
    html +=   '<span class="label label-danger apply-url-button">Apply</span>'
    html +=   '<div class="image-block-body">'
    html +=     '<img src="/assets/default.png" alt="/assets/default.png">'
    html +=   '</div>'
    html +=   '<div class="image-block-cover"></div>'
    html += '</div>'
    return html;
  }

  function adjustCss(){
    self.$block.css('top', self.$parent.css('padding-top').pixelToInt());
    self.$block.css('left', self.$parent.css('padding-left').pixelToInt());
  }

  adjustCss()

  // upload image url toggle
  var uploadToggle = Utility.toggleGenerator(function(){
    self.$uploadInput.show();
    self.$uploadButton.show();
  },function(){
    self.$uploadInput.hide().val("");
    self.$uploadButton.hide();
  })

  // activeBlock
  function activeBlock(){
    self.$body.on('click', function(e){
      e.stopPropagation();
      manager.activate(self);
    })
  }
  activeBlock();
  // icon handler:

  function handleCloseBlock(){
    self.$closeButton.on('mouseenter', function(){
      $(this).addClass('fa-2x');
    }).on('mouseleave', function(){
      $(this).removeClass('fa-2x');
    }).on('click', function(){
      self.$block.remove();
      self.manager.removeBlock(self);
    })
  }
  handleCloseBlock();

  function handleUploadUrl(){
    self.$uploadUrl.on('mouseenter', function(){
      $(this).addClass('fa-2x');
    }).on('mouseleave', function(){
      $(this).removeClass('fa-2x');
    }).on('click', function(){
      uploadToggle(true);
      self.$uploadInput.focus()
      self.$uploadButton.off().on('click', function(){
        self.$img.attr('src', self.$uploadInput.val());
        uploadToggle(false);
      });
    })
  }
  handleUploadUrl();


  function handleUploadFile(){
    self.$uploadUrl.on('mouseenter', function(){
      $(this).addClass('fa-2x');
    }).on('mouseleave', function(){
      $(this).removeClass('fa-2x');
    }).on('click', function(){

    })
  }
  handleUploadFile();

  // resize handler
  function handleBlockResizing(){
    self.$block.resizable({
      containment: '#' + self.$parent.attr('id'),
    })
  }
  handleBlockResizing();

  // drag drop handler
  function handleBlockDragging(){
    self.$block.draggable({
      handle: '.image-block-body',
      containment: '#' + self.$parent.attr('id'),
    }).on('drag', function(){
      self.$toolbar.show();
    }).css('position', 'absolute');
  }
  handleBlockDragging();

  // toggle
  var toggle = Utility.toggleGenerator(function(){
    self.$closeButton.show();
    self.$uploadUrl.show();
    self.$uploadFile.show();
    self.$block.find('.ui-resizable-se').show();
  },function(){
    self.$closeButton.hide();
    self.$uploadUrl.hide();
    self.$uploadFile.hide();
    self.$block.find('.ui-resizable-se').hide();
  });

  // toolbar
  function showHideButtons(){
    self.$block.on('mouseenter', function(){
      toggle(true);
    }).on('mouseleave', function(){
      toggle(false);
      uploadToggle(false);
    });
  }
  showHideButtons();

}


ImageBlock.prototype.copyTo = function(textBlock){
  var newImageBlock = textBlock.createImageBlock();
  newImageBlock.applyCss(this.cssProperties());
  newImageBlock.$img.attr('src', this.$img.attr("src"));
  return newImageBlock;
}

// apply css properties
ImageBlock.prototype.applyCss = function(properties){
  this.$body.css('border', properties.border);
  this.$body.css('border-radius', properties.borderRadius);
  this.$block.css('top', properties.top);
  this.$block.css('left', properties.left);
  this.$body.css('background', properties.background);
  this.$block.outerWidth(properties.outerWidth);
}

ImageBlock.prototype.cssProperties = function(){
  properties = {};
  properties['outerHeight'] = this.$block.outerHeight();
  properties['outerWidth'] = this.$block.outerWidth();
  properties['innderWidth'] = this.$block.innerWidth();
  properties['background'] = this.$body.css('background');
  properties.top = this.$block.css('top');
  properties.left = this.$block.css('left');
  properties['border'] = this.$body.css('border');
  properties['borderRadius'] = this.$body.css('border-radius');
  return properties;
}
ImageBlock.prototype.addClass = function(className){
  this.$block.addClass(className);
}


ImageBlock.prototype.getTreeJson = function(){
  var self = this;
  var treeId = 'image-tree-' + this.id;
  //callback when adding to the tree
  var action = function(){
    $(document).on('mouseenter', '#' + treeId, function(){
      $(this).prev().addClass('fa fa-2x');
      self.$cover.show();
    }).on('mouseleave', '#' + treeId,function(){
      $(this).prev().removeClass('fa-2x');
      self.$cover.hide();
    })
  }
  return [{
    id          : this.id,
    text        : '<span id="' + treeId +'">   Image-' + this.id + '</span>',
    icon        : 'fa fa-image',
    state       : {
      opened    : true,
      disabled  : false,
      selected  : false
    },
    children    : [],
    li_attr     : {},
    a_attr      : {}
  }, action]
}
