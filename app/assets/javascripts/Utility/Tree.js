var Tree = {};

Tree.initTree = function(children){
  this.$slideTree = $('#slide-tree');
  this.$slideTree.jstree({
    'core' :
    {
      'data' : [
          {
              'id' : 'root',
              'text' : 'Slides',
              'icon' : 'fa fa-table fa-2x',
              'state' : { 'opened' : true, 'disabled': true },
              'children' : children
          }
      ],
      'check_callback' : true
    },
    'plugins': ['dnd']
  });

  Tree.tree = Tree.$slideTree.jstree(true);
}
