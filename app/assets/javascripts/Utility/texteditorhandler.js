function MathBlock($parentDom){
  this._$parentDom = $parentDom;
  var self = this;
  this.currentMathId = null;

  // populate html:
  $parentDom.append(mathBlockHtml());
  this.$container = $parentDom.find('.math-block-container');
  this.$body = this.$container.find('.math-block-body');
  this.$mirror = this.$container.find('.math-block-mirror');
  this.$cover = this.$container.find('.math-block-cover');
  //init text editor:
  this.textEditor = new Quill(this.$body.get(0));
  this.$editorBody = this.$body.find('.ql-editor');
  this.$editorBody.blur(function(){
    self.hide();
  })
  // fix css:
  this.$container.css({
    'top': 0,
  })
  this.$container.css({
    'width': 30,
  })
  this.$body.css({
    'padding': 0,
  });
  this.textEditor.setText('$$$$');
  function mathBlockHtml(){
    var html = '';
    html += '<div class="math-block-container">'
    html +=   '<div class="math-block-cover"></div>'
    html +=   '<div class="math-block-body"></div>'
    html +=   '<span class="math-block-mirror" style="display: none"></span>'
    html += '</div>'
    return html;
  }

  function textWidthChange(){

    self.textEditor.on('text-change', function(delta, source){
      var thiss = this;
      if (source == 'user'){
        var text = this.getText();
        self.$mirror.text(text);
        $('#math-review-container').text(text.replace(/\$\$/g, ''));
        var mathField = MQ.MathField($('#math-review-container').get(0));

        self.resizeFunction($('#math-review-container').innerHeight(),
                            $('#math-review-container').width(),
                            text.replace(/\$\$/g, '').replace(/\n/g, ''));
        if (self.$mirror.css('width').pixelToInt() >= 30){
          self.$container.get(0).style.removeProperty('width');
        }
        else{
          self.$container.css('width', 30)
        }
        if ((delta.ops[1] && delta.ops[0].retain < 2) || delta.ops[1].insert == '\n'){
          self.backFunction();
          self.hide();
        }
      }
    });
  }
  textWidthChange();

  return this;
}


MathBlock.prototype.show = function(resizeFunction, backFunction, text){
  if (!text || text == ''){
    this.textEditor.setText('$$$$');
  }
  else{
    this.textEditor.setText('$$' + text +'$$');
  }
  this.$container.css('left', this._$parentDom.css('width'));
  this.resizeFunction = resizeFunction;
  this.backFunction = backFunction;
  this.$container.show();
  var selection = this.textEditor.getText().length - 3
  this.textEditor.getText();
  this.textEditor.setSelection(selection, selection)
  this.textEditor.focus();
}
MathBlock.prototype.hide = function(){
  this.currentMathId = null;
  this.$container.hide();
}

MathBlock.prototype.focus = function(){
  this.textEditor.focus();
}
MathBlock.prototype.applyPosition = function(top, left){
  this.$container.css({
    'top': top,
    'left': left
  })
}

MathBlock.prototype.getTreeJson = function(){
  var self = this;
  var treeId = 'math-block-tree-' + this._id;
  var action = function(){
    $(document).on('mouseenter', '#' + treeId, function(){
      self.$cover.show();
    }).on('mouseleave','#' + treeId, function(){
      self.$cover.hide()
    });
  }
  return [{
    id          : this._id,
    text        : '<span id="' + treeId + '"> Math block-' + this._id + '</span>',
    icon        : 'fa fa-header',
    state       : {
      opened    : true,
      disabled  : false,
      selected  : false
    },
    children    : [],
    li_attr     : {},
    a_attr      : {}
  }, action]
}
