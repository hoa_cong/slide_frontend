var Slide = function(id, manager, slideData){
  this.manager = manager
  this.id = id;
  this.$blockContainer = $('#slide-body');
  this.blockManager = new BlockManager(this);
}

Slide.prototype.getTreeJson = function(){
  return {
    id          : this.id,
    text        : '<span class>block-' + this.id + '</span>',
    icon        : 'fa fa-building-o',
    state       : {
      opened    : true,
      disabled  : false,
      selected  : false
    },
    children    : this.blockManager.getBlockTree(),
    li_attr     : {},
    a_attr      : {}
  }
}

Slide.prototype.createTextBlock = function(){
  var newBlock = new TextBlock(this.$blockContainer, this.blockManager);
  var textblockTree = newBlock.getTreeJson();
  Tree.tree.create_node(this.id, textblockTree[0], false, textblockTree[1]);
  Utility.slideTree.addChildren
  return newBlock;
}

Slide.prototype.createImageBlock = function(){
  var newBlock = new ImageBlock(this.$blockContainer, this.blockManager);
  var imageTree = newBlock.getTreeJson();
  Tree.tree.create_node(this.id, imageTree[0], false, imageTree[1]);
  return newBlock;
}
