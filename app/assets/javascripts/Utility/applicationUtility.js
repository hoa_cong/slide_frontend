//// applicaton objects

var Utility = new Object();
Utility.blockDecorator = new Object();
Utility.blockContentDecorator = new Object();

Utility.colorNames = ['#F0F8FF','#FAEBD7','#00FFFF','#7FFFD4', '#F0FFFF','#F5F5DC','#FFE4C4','#000000', '#A52A2A','#5F9EA0', '#7FFF00','#FF7F50', '#6495ED', '#FFF8DC', '#DC143C', '#008B8B', '#A9A9A9','#E9967A','#8FBC8F', '#483D8B','#2F4F4F'];
Utility.languages = ['scheme', 'python', 'ruby', 'javascript'];
Utility.fontNames = ["'Open Sans', sans-serif", "'Open Sans Condensed', sans-serif", "'Indie Flower', cursive", "'Poiret One', cursive"]
Utility.fontSizes = ['8px', '9px','10px', '11px', '13px', '15px', '17px']
Utility.blockContentDecoratorId = 'block-content-decorator';


// property is either border-size or border-width -> corresponding to class name of html element
Utility.intializeUtility = function(){
  this.initializeBlockDecorator();
  this.initializeBlockContentDecorator();

  this.togglefuncs = [this.blockDecorator.toggleBorder,
                  this.blockDecorator.toggleBackground,
                  this.blockContentDecorator.toggleFont,
                  this.blockContentDecorator.toggleFontSize,
                  this.blockContentDecorator.toggleHighlight,
                  this.blockContentDecorator.toggleFontColor]
}

// close toggle except element in list
Utility.closeToggleExcept = function(toggle){
  (this.togglefuncs).forEach(function(toggleFunc){
    if (toggleFunc !== toggle) toggleFunc(false);
  });
}

Utility.initializeBlockDecorator = function(){
  var self = this.blockDecorator;
  self.container = $('#block-decorator');

  var $borderColor = self.container.find('.border-color');
  self.borderInvoker = $borderColor.prev();
  self.borderColorOption = $borderColor.children().first();
  self.toggleBorder = this.intializeColorOption(self.borderInvoker, self.borderColorOption);

  var $backgroundColor = self.container.find('.background-color');
  self.backgroundInvoker = $backgroundColor.prev();
  self.backgroundColorOption = $backgroundColor.children().first()
  self.toggleBackground = this.intializeColorOption(self.backgroundInvoker, self.backgroundColorOption);

}


Utility.blockDecorator.handleBorderOf = function(block, property, step){
  var $borderHandler = this.container.find('.' + property);
  var $increment = $borderHandler.find('.increment');
  var $decrement = $borderHandler.find('.descrement');
  $increment.unbind('click');
  $increment.off().on('click', function(e){
    $(block).css(property, parseInt($(block).css(property)) + (step ? step : 1))
  });
  $decrement.unbind('click');
  $decrement.off().on('click', function(e){
    var border_radius =  parseInt($(block).css(property));
    if (border_radius <= 0 || (step && border_radius - step <= 0)){
      $(block).css(property, 0);
    }
    else{
      $(block).css(property, border_radius - (step ? step : 1));
    }
  })
}

Utility.blockDecorator.handleBackgroundOf = function($block, property){
  var $invoker = this[property + 'Invoker'];
  var $colorOption = this[property + 'ColorOption'];
  var toggle = this['toggle' + property.camelize()];
  $invoker.unbind('click')
  $invoker.off().on('click', function(){
    if (toggle()){
      Utility.closeToggleExcept(toggle);
      $colorOption.children().each(function(index, colorElement){
        $(colorElement).unbind('click')
        $(colorElement).off().on('click', function(){
          $block.css(property + '-color', $(this).css('background-color'));
        })
      });
    };
  });
}

Utility.bindBlockDecoratorTo = function($block){
  var self = this.blockDecorator;
  self.toggleBorder(false);
  self.toggleBackground(false);
  self.handleBorderOf($block, 'border-radius');
  self.handleBorderOf($block, 'border-width');
  self.handleBackgroundOf($block, 'border');
  self.handleBackgroundOf($block, 'background');

}

// block content decorator
Utility.initializeBlockContentDecorator = function(){
  var self = this.blockContentDecorator;
  self.container = $('#block-content-decorator');


  //intialize font style
  self.fontOption = $(self.container.find('.font-style-options'))
  self.fontInvorker = self.fontOption.prev();
  self.toggleFont = this.intializeFontOption(self.fontInvorker, self.fontOption);

  //intialize font color

  var $fontColor = self.container.find('.font-text-color');
  self.fontColorInvoker = $fontColor.prev();
  self.fontColorOption = $fontColor.children().first()
  self.toggleFontColor = this.intializeColorOption(self.fontColorInvoker, self.fontColorOption);


  //initialize highlight

  var $highlight = self.container.find('.font-text-hightlight');
  self.highlightInvoker = $highlight.prev();
  self.highlightColorOption = $highlight.children().first();
  self.toggleHighlight = this.intializeColorOption(self.highlightInvoker, self.highlightColorOption);

  //intialize size

  self.fontSizeOption = self.container.find('.font-text-size');
  self.sizeInvoker = self.fontSizeOption.prev();
  self.toggleFontSize = this.intilizeFontSizeOption(self.sizeInvoker, self.fontSizeOption);

  // intialize basic decorator : bold, italic ....
  self.basicDecorator = {};
}

Utility.bindBlockContentDecoratorTo = function(text_editor, id){
  var self = this.blockContentDecorator;
  this.closeToggleExcept();
  self.handleFontOf(text_editor);
  self.handleFontColorOf(text_editor);
  self.handleHighlightOf(text_editor);
  self.handleFontSizeOf(text_editor);
  self.hideAllQuillToolbar();
  self.showQuillToolbar(id);
}


Utility.blockContentDecorator.handleFontSizeOf = function(text_editor){
  var $invoker = $(this.sizeInvoker);
  var $sizeOption = $(this.fontSizeOption);
  var toggle = this.toggleFontSize;
  $invoker.off().on('click',function(){
    text_editor.focus();
    if (toggle()){
      Utility.closeToggleExcept(toggle);
      $sizeOption.children().each(function(index, fontElement){
        $(fontElement).off().on('click', function(){
          text_editor.focus();
          var selection = text_editor.getSelection();
          var start = selection.start;
          var end = selection.end;
          if (start === end){
            text_editor.prepareFormat('size', $(this).attr('data-value'));
          }
          else {
            try{
              text_editor.formatText(start, end, {
                'size': $(this).attr('data-value')
              });
            }
            catch(e){
              // nothing
            }
          }
        })
      })
    }
  })

}

Utility.blockContentDecorator.handleHighlightOf = function(text_editor){
  var $invoker = $(this.highlightInvoker);
  var $colorOption = $(this.highlightColorOption);
  var toggle = this.toggleHighlight;
  $invoker.off().on('click', function(){
    text_editor.focus();
    if (toggle()){
      Utility.closeToggleExcept(toggle)
      $colorOption.children().each(function(index, colorElement){
        $(colorElement).unbind('click')
        $(colorElement).off().on('click', function(){
          text_editor.focus();
          var selection = text_editor.getSelection();
          var start = selection.start;
          var end = selection.end;
          if (start === end){
            text_editor.prepareFormat('background', $(this).css('background-color'));
          }
          else {
            try{
              text_editor.formatText(start, end, {
              'background': $(this).css('background-color')
              })
            }
            catch(e){
              // nothing
            }
          }
        })
      });
    };
  });
}

Utility.blockContentDecorator.handleFontColorOf = function(text_editor){
  var $invoker = $(this.fontColorInvoker);
  var $colorOption = $(this.fontColorOption);
  var toggle = this.toggleFontColor;
  $invoker.off().on('click', function(){
    text_editor.focus();
    if (toggle()){
      Utility.closeToggleExcept(toggle)
      $colorOption.children().each(function(index, colorElement){
        $(colorElement).unbind('click')
        $(colorElement).off().on('click', function(){
          text_editor.focus();
          var selection = text_editor.getSelection();
          var start = selection.start;
          var end = selection.end;
          if (start === end){
            text_editor.prepareFormat('color', $(this).css('background-color'));
          }
          else {
            try{
              text_editor.formatText(start, end, {
                'color': $(this).css('background-color')
              });
            }
            catch(e){
              // nothing
            }
          }
        })
      });
    };
  });
}

Utility.blockContentDecorator.handleFontOf = function(text_editor){
  var $invoker = $(this.fontInvorker);
  var $fontOption = $(this.fontOption);
  var toggle = this.toggleFont;
  $invoker.off().on('click',function(){
    text_editor.focus();
    if (toggle()){
      Utility.closeToggleExcept(toggle)
      $fontOption.children().each(function(index, fontElement){
        $(fontElement).off().on('click', function(){
          text_editor.focus();
          var selection = text_editor.getSelection();
          var start = selection.start;
          var end = selection.end;
          if (start === end){
            text_editor.prepareFormat('font', $(this).attr('data-value'));
          }
          else {
            try{
              text_editor.formatText(start, end, {
                'font': $(this).attr('data-value')
              })
            }
            catch(e){
              // nothing
            }
          }
        })
      })
    }
  })
}



// Utility that can use anywhere
Utility.intilizeFontSizeOption = function($invoker, $toggleElement){
  Utility.fontSizeUtility($toggleElement);
  return Utility.toggleGenerator(
                      function(){
                        $toggleElement.show(200);
                      },
                      function(){
                        $toggleElement.hide();
                      });
}

Utility.intializeColorOption = function($invoker, $toggleElement){
  Utility.colorUtility($toggleElement);
  return Utility.toggleGenerator(
                      function(){
                        $invoker.removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
                        $toggleElement.show(200);
                      },
                      function(){
                        $invoker.removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
                        $toggleElement.hide();
                      });
}

Utility.intializeFontOption = function($invoker, $toggleElement){
  Utility.fontUtility($toggleElement);
  return Utility.toggleGenerator(
                      function(){
                        $toggleElement.show();
                      },
                      function(){
                        $toggleElement.hide();
                      });
}

Utility.fontSizeUtility = function($container){
  this.fontSizes.forEach(function(size){
    var sizeElement = document.createElement('span');

    //add attributes:
    sizeElement.setAttribute('class', 'size-element');
    sizeElement.setAttribute('data-value', size);

    //add text value
    sizeElement.innerHTML = "Text";

    //add styling:
    sizeElement.style.padding = '0';
    sizeElement.style.margin = '0';
    sizeElement.style.fontSize = size;
    sizeElement.style.height = '30px';
    sizeElement.style.width = '30px';

    $container.append(sizeElement);
  })
}

Utility.fontUtility = function($container){
  this.fontNames.forEach(function(font){
    var fontElement = document.createElement('span')

    // add attributes
    fontElement.setAttribute('class', 'font-element');
    fontElement.setAttribute('data-value', font);

    // add text value, use innerHTML in case it's in form of html
    fontElement.innerHTML = "Text"
    // add styling
    fontElement.style.padding = '0';
    fontElement.style.margin = '0';
    fontElement.style.fontFamily = font;
    fontElement.style.fontSize = '30px';
    fontElement.style.float = 'left';
    //fontElement.style.height = '32px';
    //fontElement.style.width ='32px';
    //fontElement.style.border = '1px solid black'
    $container.append(fontElement)
  });
}

Utility.colorUtility = function($element){
  this.colorNames.forEach(function(color){
    // pure js style
    var colorElement = document.createElement('div');
    colorElement.setAttribute('class', 'color-element');
    colorElement.setAttribute('value', color);

    // css is set here for easing debugging or fixing
    colorElement.style.padding = '0';
    colorElement.style.margin = '0';
    colorElement.style.border = '1px solid black'
    colorElement.style.height = '40px';
    colorElement.style.width ='40px';
    colorElement.style.backgroundColor = color;
    colorElement.style.float = 'left';
    // add hover event
    $(colorElement).hover(function(){
      $(this).css('border-width', 0);
    }).on('mouseleave', function(){
      $(this).css('border-width', 1);
    })
    $element.append(colorElement);
  })

}

Utility.toggleGenerator = function(toggleAction, defaultAction){
  var bool = true;
  //return a function that takes a boolean
  return function(setting){
    if (typeof setting != 'undefined'){
      bool = setting;
    }
    if (bool){ //toggle action return the value to decide what's next
      toggleAction.apply(this, arguments);
      bool = false;
      return true;
    }
    else{
      defaultAction.apply(this, arguments);
      bool = true;
      return false;
    }
  }
}

Utility.createQuillToolbar = function(id){
  var $basicDecorator = $('#basic-decorators-template');
  var $container = $basicDecorator.parent();
  var $copiedDecorator = $basicDecorator.clone();
  $copiedDecorator.attr('id','basic-decorators-' + id);
  $container.append($copiedDecorator);
  this.blockContentDecorator.basicDecorator[id] = $copiedDecorator;
  return $copiedDecorator;
}

Utility.blockContentDecorator.hideAllQuillToolbar = function(){
  for (var key in this.basicDecorator){
    this.basicDecorator[key].hide();
  }
}

Utility.blockContentDecorator.showQuillToolbar = function(id){
  this.basicDecorator[id].show();
}

Utility.blockContentDecorator.deleteQuillToolbar = function(id){
  var obj = this.basicDecorator[id]
  delete this.basicDecorator[id];
  if (obj){
    obj.remove();
  }
}

Utility.max = function(num1, num2){
  if (num1 > num2)
    return num1;
  else
    return num2;
}


/// Utility Tree

Utility.initFancyTree = function(treeId){
  this.treeId = treeId;
  $('#' + treeId).fancytree();
  this.slideTree = $('#' + treeId).fancytree('getTree');
  this.rootNode = this.slideTree.getRootNode();
}

Utility.addTreeNode = function (newNode, parent, callbacks){
  
}
